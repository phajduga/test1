var Validator = function() {
    this.validateIBAN = function(iban) {
        var newIban = iban.toUpperCase(),
        modulo = function (divident, divisor) {
            var cDivident = '';
            var cRest = '';
             
            for (var i in divident ) {
                var cChar = divident[i];
                var cOperator = cRest + '' + cDivident + '' + cChar;
                 
                if ( cOperator < parseInt(divisor) ) {
                    cDivident += '' + cChar;
                } else {
                    cRest = cOperator % divisor;
                    if ( cRest == 0 ) {
                        cRest = '';
                    }
                    cDivident = '';
                }
            }
            cRest += '' + cDivident;
            if (cRest == '') {
                cRest = 0;
            }
            return cRest;
        };
         
        if (newIban.search(/^[A-Z]{2}/gi) < 0) {
            return false;
        }
         
        newIban = newIban.substring(4) + newIban.substring(0, 4);
     
        newIban = newIban.replace(/[A-Z]/g, function (match) {
            return match.charCodeAt(0) - 55;
        });
             
        return parseInt(modulo(newIban, 97), 10) === 1;
    };

    this.validateNRB = function(nrb) {
        nrb = nrb.replace(/[^0-9]+/g, '');
        var wagi = new Array(1, 10, 3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51, 25, 56, 75, 71, 31, 19, 93, 57);

        if (nrb.length == 26) {
            nrb = nrb + "2521";
            nrb = nrb.substr(2) + nrb.substr(0, 2);
            var Z = 0;
            for (var i=0; i<30; i++) {
                Z += nrb[29 - i] * wagi[i];
            }
            if (Z % 97 == 1) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    };

    this.validateAmount = function(money) {
        if (_.isNull(money.amount.match('^[-+]?[0-9]+$')))
            return false;
        if (_.isNull(money.currency.toLowerCase().match('^[a-z]{3}$')))
            return false;
        return true;
    };

    this.validatePhone = function(phone) {
        if (_.isNull(phone.country.match('^[0-9]{2,3}$')))
            return false;
        if (_.isNull(phone.area.match('^[0-9]{2}$')))
            return false;
        if (_.isNull(phone.number.match('^[0-9]{7}$')))
            return false;
        return true;
    };

    this.validateMobile = function(phone) {
        if (_.isNull(phone.country.match('^[0-9]{2,3}$')))
            return false;
        if (_.isNull(phone.number.match('^[0-9]{9}$')))
            return false;
        return true;
    };
}
