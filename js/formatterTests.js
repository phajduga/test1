module('Formatter');

test('format IBAN', function() {
    var formatter = new Formatter({ IBANSeparator: ' ' });

    equal(formatter.formatIBAN('PL12341234123412341234123412'), 'PL12 3412 3412 3412 3412 3412 3412');
    equal(formatter.formatIBAN('EN09870987098709870987'), 'EN09 8709 8709 8709 8709 87');
});

test('format NRB', function() {
    var formatter = new Formatter({ NRBSeparator: ' ' });

    equal(formatter.formatNRB('CCAAAAAAAABBBBBBBBBBBBBBBB'), 'CC AAAA AAAA BBBB BBBB BBBB BBBB');
});

test('format amount', function() {
    var formatter = new Formatter({
        amountNumberSeparator: ' ',
        amountFractionSeparator: ',',
    });

    equal(formatter.formatAmount({amount: 12341234, currency: 'pln'}),
        '+123 412,34 PLN');
    equal(formatter.formatAmount({amount: 234987, currency: 'EUR'}),
        '+2 349,87 EUR');
    equal(formatter.formatAmount({amount: -234987, currency: 'EUR'}),
        '-2 349,87 EUR');
    equal(formatter.formatAmount({amount: -45234987, currency: 'EUR'}),
        '-452 349,87 EUR');
    equal(formatter.formatAmount({amount: 87, currency: 'EUR'}),
        '+0,87 EUR');
    equal(formatter.formatAmount({amount: 1, currency: 'PLN'}),
        '+0,01 PLN');
    equal(formatter.formatAmount({amount: -1, currency: 'PLN'}),
        '-0,01 PLN');
    equal(formatter.formatAmount({amount: 0, currency: 'EUR'}),
        '+0,00 EUR');
});

test('format phone', function() {
    var formatter = new Formatter({
        phoneSeparator: '-',
        phonePrefix: '+',
    });
    
    equal(formatter.formatPhone({country: '48', area: '22', number: '7182345'}),
        '+48 22 718-23-45');
});

test('format mobile', function() {
    var formatter = new Formatter({
        phoneSeparator: '-',
        phonePrefix: '+',
    });
    
    equal(formatter.formatMobile({country: '48', number: '884304230'}),
        '+48 884-304-230');
});

test('format string as separated groups of characters', function() {
    var formatter = new Formatter({});

    equal(formatter.separateFromBack('1111111111', 4, '-'), '11-1111-1111');
});
