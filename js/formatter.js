var Formatter = function(config) {
    this.IBANSeparator = ' ';
    this.NRBSeparator = ' ';
    this.phoneSeparator = '-';
    this.phonePrefix = '+';
    this.amountNumberSeparator = '.';
    this.amountFractionSeparator = ',';

    this.configure = function(config) {
        _.extend(this, config);
    };

    this.formatIBAN = function(IBAN) {
        var result = [];
        for (var i=0; i<IBAN.length; i+=4)
            result.push(IBAN.toUpperCase().substr(i, 4));
        return result.join(this.IBANSeparator);
    };

    this.formatNRB = function(NRB) {
        var result = [ NRB.substr(0, 2), ];
        for(var i=2; i<NRB.length; i+=4)
            result.push(NRB.toUpperCase().substr(i, 4));
        return result.join(this.NRBSeparator);
    };

    this.formatAmount = function(amount) {
        var fraction = amount.amount % 100;
        var whole = Math.floor((amount.amount - fraction) / 100);
        var sign = amount.amount<0 ? '-' : '+';
        var result = sign + this.separateFromBack(String(Math.abs(whole)), 3, this.amountNumberSeparator);
        fraction = String(Math.abs(fraction));
        while (fraction.length < 2) fraction = "0" + fraction;
        result += this.amountFractionSeparator + fraction;
        result += ' ' + amount.currency.toUpperCase();
        return result;
    };

    this.formatPhone = function(phone) {
        var result = this.phonePrefix + phone.country;
        result += ' ' + phone.area + ' ';
        result += [
            phone.number.substr(0, 3),
            phone.number.substr(3, 2),
            phone.number.substr(5, 2)
        ].join(this.phoneSeparator);
        return result;
    };

    this.formatMobile = function(mobile) {
        var result = this.phonePrefix + mobile.country;
        result += ' ' + this.separateFromBack(
                mobile.number, 3, this.phoneSeparator);
        return result;
    };

    this.separateFromBack = function(str, characters, separator) {
        var i_start = str.length % characters;
        var groups = [];
        if (i_start > 0)
            groups.push(str.substr(0, i_start));
        for (var i=i_start; i<str.length; i+=characters)
            groups.push(str.substr(i, characters));
        return groups.join(separator);
    };

    // initialization
    _.extend(this, config);
}
