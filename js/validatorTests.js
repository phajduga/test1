module('Validator', {
    setup: function() {
        validator = new Validator();
    }
});

test('validate IBAN', function() {
    // wrong IBAN length should validate false
    equal(validator.validateIBAN('PL1234123123412341245674567'), false);
    // wrong IBAN checksum should validate false
    equal(validator.validateIBAN('PL12341231234123412456374567'), false);
    // correct IBAN checksum should validate true
    equal(validator.validateIBAN('PL27114020040000300201355387'), true);
});

test('validate NRB', function() {
    equal(validator.validateNRB('34572153645'), false);
    equal(validator.validateNRB('27114020040000301201355387'), false);
    equal(validator.validateNRB('27114020040000300201355387'), true);
});

test('validate amount', function() {
    equal(validator.validateAmount({amount: '', currency: 'PLN'}), false);
    equal(validator.validateAmount({amount: '1234fsa', currency: 'PLN'}), false);
    equal(validator.validateAmount({amount: '43', currency: 'LN'}), false);
    equal(validator.validateAmount({amount: '43', currency: '0as'}), false);
    equal(validator.validateAmount({amount: '-123', currency: 'PLN'}), true);
    equal(validator.validateAmount({amount: '+123', currency: 'PLN'}), true);
    equal(validator.validateAmount({amount: '123', currency: 'PLN'}), true);
    equal(validator.validateAmount({amount: '123', currency: 'pln'}), true);
});

test('validate phone', function() {
    equal(validator.validatePhone({country: 'iu', area: '32', number: '4567890'}), false);
    equal(validator.validatePhone({country: '48', area: 'as', number: '4567890'}), false);
    equal(validator.validatePhone({country: '48', area: '32', number: '456u890'}), false);
    equal(validator.validatePhone({country: '48', area: '', number: '4567890'}), false);
    equal(validator.validatePhone({country: '48', area: '32', number: '4567890'}), true);
});

test('validate mobile', function() {
    equal(validator.validateMobile({country: '', number: '884304230'}), false);
    equal(validator.validateMobile({country: '48', number: '884g4230'}), false);
    equal(validator.validateMobile({country: '48', number: '8844230'}), false);
    equal(validator.validateMobile({country: '1000', number: '884304230'}), false);
    equal(validator.validateMobile({country: '48', number: '884304230'}), true);
});
